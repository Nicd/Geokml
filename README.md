# Geokml
Suomen kuntarajat, graticulet ja reittipisteet - Geocaching kml map

[![Esim.png](https://raw.githubusercontent.com/geoharo/Geokml/master/MyMapsEsim.png)](https://drive.google.com/open?id=114VJTemi07NH27FAre7fAtSISjk&usp=sharing)


<b>[>>>Esimerkkikartta<<<](https://drive.google.com/open?id=114VJTemi07NH27FAre7fAtSISjk&usp=sharing)</b>


[Juttu työkalusta weellun 61° 23° Tampere -blogissa](https://www.6123tampere.com/2016/08/15/tyokalu-kuntakartan-varittamiseen/)




Tästä GitHub-reposta löydät tarvittavat välineet ja ohjeet löytöjen mukaan väritetyn kuntakartan ja graticulekartan luomiseen sekä reittipisteiden lisäämiseen Google Mapsiin.


<br>Tarvitset scriptin ajamiseen joko MS Excelin tai [Pythonin](https://www.python.org/downloads/), sekä Google-tilin online tallennusta varten.

Vaihtoehtoisesti voit myös [luoda kuntakartan nettiselaimessa](https://repl.it/FNVg/0) ilman mitään asennuksia, mutta joudut kopioimaan outputin käsin ja myös kml-tiedoston luominen täytyy tehdä manuaalisesti.<br>


Perustuu Maanmittauslaitoksen aineistoon.


Maanmittauslaitoksen kuntarajat WMS-palveluna (esimerkiksi Locus Map -sovellukseen) – WMS-url: http://avoindata.maanmittauslaitos.fi/geoserver/ows?service=wms&version=1.1.1&request=GetCapabilities
<br><br>
<b>Helpoin tapa oman Kuntakartta.kml:n luomiseen: [Kuntakartta to kml](https://openuserjs.org/scripts/geoharo/Kuntakartta_to_kml)</b><br><br>

Katso myös [GeoUserscripts](https://openuserjs.org/users/geoharo/scripts)

Tarvittaessa [ota yhteyttä!](https://www.geocaching.com/email/?guid=d30ee7cc-018f-4e64-a4b1-06c4011e4f63)

-geohärö
